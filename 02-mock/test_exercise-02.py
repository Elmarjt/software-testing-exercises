"""
When DB.get() is called:
    - With non-existent table_name it raises a KeyError
    - With non-existent index it raises an IndexError

Use Mock() with side_effect to make the tests in 02-mock/test_exercise-02.py run faster.
"""
import pytest
from unittest.mock import Mock

@pytest.fixture()
def db_mock():
    db = Mock()
    db.count.return_value = 1
    db.list_tables.return_value = ("users")
    db.get.return_value = {
        "username": "drake123",
        "first_name": "Daniel",
        "last_name": "Smith",
        "email": "drake123@yahoo.com",
    }
    return db

def test_count(db_mock):
    assert db_mock.count() == 1

def test_list_tables(db_mock):
    assert db_mock.list_tables() == ("users")

def test_get(db_mock):
    assert db_mock.get("users")["username"] == "drake123"